# App Answerly (local)
From the book "Building-Django-2.0-Web-Applications" by Tom Aratyn

## Description
For reference. The codes mentioned in book have a few mistakes and they are corrected in this repository. Sample codes of app "Answerly" from chapter 6-8, which is for local development.

- finalized codes at the end of chapter 8, not separated chapter by chapter

- scripts_ref/terminal_cmd.txt describes the terminal commands (i.e. commands run in console/terminals) used from chapter 6-8

- elasticsearch version used is 7.7.0, so some codes are different from the book
