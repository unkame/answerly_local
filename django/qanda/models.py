from django.db import models
from django.conf import settings
from django.urls.base import reverse

from qanda.service import elasticsearch


class Question(models.Model):
    title = models.CharField(max_length=140)
    question = models.TextField()
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, 
                             on_delete=models.CASCADE
                            )
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        # The code in book is "questions:question_detail", but the app name is "qanda"
        return reverse('qanda:question_detail', kwargs={'pk':self.id})

    # fat model
    def can_accept_answers(self, user):
        return user == self.user

    def as_elasticsearch_dict(self):
        '''
        return a dict suitable for loading into ElasticSearch 
        '''
        return {
            '_id': self.id,         # id of Elasticsearch document
            #'_type': 'doc',        # document's mapping type, deprecated in version 7.x.x
            'text': '{}\n{}'.format(self.title, self.question),
            'question_body': self.question,
            'title': self.title,
            'id': self.id,
            'created': self.created,
        }
    
    # override save method, which is used by CreateView, UpdateView, 
    # QuerySet.create() & Manager.create()
    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert=force_insert,
                     force_update=force_update,
                     using=using,
                     update_fields=update_fields)
        elasticsearch.upsert(self)      # also update elasticsearch


class Answer(models.Model):
    answer = models.TextField()
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL,
                             on_delete=models.CASCADE                             
                            )
    created = models.DateTimeField(auto_now_add=True)
    question = models.ForeignKey(to=Question, 
                                 on_delete=models.CASCADE
                                )       #model Question could call "answer_set"
    accepted = models.BooleanField(default=False)

    class Meta:
        ordering = ('-created',)