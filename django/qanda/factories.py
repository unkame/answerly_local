from unittest.mock import patch
import factory
from elasticsearch import Elasticsearch

from qanda.models import Question
from user.factories import UserFactory

class QuestionFactory(factory.DjangoModelFactory):
    title = factory.Sequence(lambda n: 'Question #%d' % n)
    question = 'What is a question?'
    user = factory.SubFactory(UserFactory)  #use User creared in UserFactory

    class Meta:
        model = Question

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # mock elasticsearch, since it creates Question object
        with patch('qanda.service.elasticsearch.Elasticsearch'):
            question = super()._create(model_class, *args, **kwargs)
        return question
