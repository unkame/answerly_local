from django.contrib.auth.forms import UserCreationForm
from django.views.generic import CreateView
from django.contrib.auth import login, authenticate
from django.http import HttpResponseRedirect
from django.urls import reverse

class RegisterView(CreateView):
    template_name = 'user/register.html'
    form_class = UserCreationForm

    # Book not mentioned
    # Auto-login after success registration
    def get_success_url(self):
        return reverse('qanda:index')

    def form_valid(self, form):
        #save the new user first
        #UserCreationForm shall already validate the form 
        form.save()
        #get the username and password
        username = self.request.POST['username']
        password = self.request.POST['password1']   # password1, not password
        #authenticate user then login
        user = authenticate(username=username, password=password)
        login(self.request, user)
        return HttpResponseRedirect(self.get_success_url())
        